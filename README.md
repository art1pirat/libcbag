libcbag
=======

General
-------

C++ Library to handle BagIt structures. BagIt is a standard format to create transfer packages for digital preservation purposes.
See https://en.wikipedia.org/wiki/BagIt for details

The library supports BagIt in version 1.0, yet (also previous versions).

It is licensed under terms of GNU General Public License, v3 or higher, see
file License for details.

The main website of the project author is http://andreas-romeyke.de. Please do
not hesitate to contact me if you find problems or give hints.

Principle
---------

The library only checks a given or created BagIt structure if you call
"validate()". This allows us to prepare or read incomplete structures.

If you want to copy a BagIt from source to destination, you should create a
bag-object first. Then you should use validate() and then call store().

If you want to generate a new BagIt structure, you need to define a directory
path first. Then you create the sub objects for a Bag using the defined
directory as base. For each of this objects call required methods and at least
the store()-call. Then you create the Bag object and call store().

