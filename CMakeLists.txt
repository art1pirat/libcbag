cmake_minimum_required (VERSION 2.6)
project (libcbag)

# add default CFLAGS
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}  -std=c++11 -fstack-protector-strong -Wformat -Werror=format-security")
#set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -Wl,-z,relro -fPIE")

add_subdirectory(src)
# TESTS
add_subdirectory(test)
enable_testing()

add_test(NAME TestPayload COMMAND TestPayload)
add_test(NAME TestPayloadManifest COMMAND TestPayloadManifest)
add_test(NAME TestTagManifest COMMAND TestTagManifest)
add_test(NAME TestBagMetadata COMMAND TestBagMetadata)
add_test(NAME TestFetchFile COMMAND TestFetchFile)
add_test(NAME TestBag COMMAND TestBag)
# cpack definitions
INCLUDE(InstallRequiredSystemLibraries)
set (CPACK_PACKAGE_VENDOR "Andreas Romeyke")
set (CPACK_VERSION_MAJOR "0")
set (CPACK_RESOURCE_FILE_LICENSE "${PROJECT_SOURCE_DIR}/License")
set (CPACK_RESOURCE_FILE_README "${PROJECT_SOURCE_DIR}/README")
set (CPACK_PACKAGE_DESCRIPTION_FILE "${PROJECT_SOURCE_DIR}/README.md")
set (CPACK_GENERATOR="TGZ;DEB;RPM")
#set (CPACK_GENERATOR="DEB")
set (CPACK_PACKAGE_CONTACT "Andreas Romeyke libcbag@andreas-romeyke.de")
set(CPACK_DEBIAN_PACKAGE_SHLIBDEPS ON)
set(CPACK_DEBIAN_PACKAGE_DEPENDS "libssl1.1 (>= 1.1.1), libcurl4 (>=7.64.0)")
# TODO: set(CPACK_PACKAGE_EXECUTABLES)
## INSTALL
# Binaries
install (TARGETS create_a_bag RUNTIME DESTINATION bin)
install (TARGETS validate_a_bag RUNTIME DESTINATION bin)

# Library
# Note: may not work on windows
install (TARGETS cbag
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib
  PUBLIC_HEADER DESTINATION include
) 


include(CPack)
