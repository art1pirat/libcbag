﻿Author: Max Mustermann
Payload-Oxum: 128.1
Bagging-Date: 2019-11-27
Bag-Size: 0,4 KB
External-Description: Dies ist ein kleines Beispiel für eine IE, die als SIP im BagIt-Format des SLUBArchivs eingeliefert werden soll.
External-Identifier: testbag
SLUBArchiv-archivalValueDescription: Gesetzlicher Auftrag der SLUB Dresden
SLUBArchiv-exportToArchiveDate: 20191127T120000.00
SLUBArchiv-externalId: 99193991991991
SLUBArchiv-externalIsilId: DE-14
SLUBArchiv-externalWorkflow: kitodo
SLUBArchiv-hasConservationReason: true
SLUBArchiv-rightsVersion: 1.0
SLUBArchiv-sipVersion: v2020.1
Title: BeispielIE