// Copyright (C) 2018 Andreas Romeyke (art1@andreas-romeyke.de), 2018.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include "payload.hpp"
#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/filesystem/operations.hpp>
#include <list>
#include <string>
//#include <filesystem> // c++17

//namespace fs = std::filesystem;
namespace fs = boost::filesystem;
using namespace std;
Payload::Payload( const string& tmpbasedir ) {
  this->basedir = tmpbasedir;
  fs::path p{ this->basedir };
  // fs::file_status s = fs::status( p );
  // log << "tmpbasedir "<< p.string() << endl;
  // log << "is dir: "<< fs::is_directory( s) << endl;
  // log << "exists: "<< fs::exists(s) << endl;
};

void Payload::scan_dir_recursively( const fs::path& directory, list<fs::path> &paths) {
  fs::recursive_directory_iterator iter(directory);
  fs::recursive_directory_iterator end;
  for (; iter != end; ++iter) {
    fs::path subdir = iter->path();
    paths.push_back( subdir );
  }
}

// get all relative paths relative to the bag base directory
list<string> Payload::get_all_relative_paths() {
  list<fs::path> paths;
  list<string> strpaths;
  fs::path directory(this->basedir + "/data");
  if (fs::is_directory( directory )) {
    Payload::scan_dir_recursively( directory, paths);
    for (auto & path : paths) {
      if (fs::is_regular_file( path )) {
        fs::path relpath = fs::relative(path, this->basedir);
        strpaths.push_back( relpath.string() );
      }
    }
  }
  return strpaths;
}

list<string> Payload::get_all_absolute_paths() {
  list<fs::path> paths;
  list<string> strpaths;
  fs::path directory(this->basedir + "/data");
  if (fs::is_directory( directory )) {
    Payload::scan_dir_recursively( directory, paths);
    for (auto & path : paths) {
      if (fs::is_regular_file( path )) {
        strpaths.push_back( path.string() );
      }
    }
  }
  return strpaths;
}

bool Payload::import_data_dir( const string& importdir ) {
    fs::path b{ basedir };
    fs::file_status bs = fs::status( b );
    if (! fs::is_directory( bs ) ) {
        fs::create_directory(b);
    }
    string newbasedir = basedir + "data/";
	fs::path p{ newbasedir };
	fs::file_status s = fs::status( p );
	if (fs::is_directory( s)) {
		// TODO(art1): Payload::log << "directory '" << newbasedir << "'already exists" << endl;
		cout << "directory '" << newbasedir << "'already exists" << endl;
		return false;
	}
    fs::create_directory(p);
    list<fs::path> paths;
    Payload::scan_dir_recursively( importdir, paths);
    for (auto & path : paths) {
        fs::path relpath = fs::relative(path, importdir);
		string sourcepath = path.string();
		string targetpath = newbasedir + relpath.string();
		cout << "map '" << sourcepath << "' to '" << targetpath << "'" <<endl;
		fs::path from_fp{ sourcepath };
		fs::path to_fp{ targetpath };
		fs::path parentdir = to_fp.parent_path();
		if (! fs::is_directory (parentdir)) {
			fs::create_directory( parentdir );
		}
		if (fs::is_directory( from_fp ) ) {
            fs::create_directory( to_fp );
        } else {
            fs::copy_file (from_fp, to_fp);
        }
	}
	return true;
}

bool Payload::validate() {
  fs::path directory(this->basedir + "/data");
  // TODO: check if all files are relative to directory path
  return fs::is_directory( directory );
}


bool Payload::store( const string& tmpbasedir ) {
    string newbasedir = tmpbasedir + "data/";
	fs::path p{ newbasedir };
	fs::file_status s = fs::status( p );
	if (fs::is_directory( s)) {
		// TODO(art1): Payload::log << "directory '" << newbasedir << "'already exists" << endl;
		log << "directory '" << newbasedir << "'already exists" << endl;
		return false;
	}
    fs::create_directory(p);
	list<string> paths = Payload::get_all_relative_paths();
	for (const string& relpath : paths) {
		string sourcepath = Payload::basedir + relpath;
		string targetpath = tmpbasedir + relpath;
		log << "map '" << sourcepath << "' to '" << targetpath << "'" <<endl;
		fs::path from_fp{ sourcepath };
		fs::path to_fp{ targetpath };
		fs::path parentdir = to_fp.parent_path();
		if (! fs::is_directory (parentdir)) {
			fs::create_directory( parentdir );
		}
		fs::copy_file (from_fp, to_fp);
	}

	return false;
}
void Payload::get_logstream( stringstream & log ) {
	log << this->log.rdbuf();
}

void Payload::reset_logstream() {
	this->log.str(std::string());
}

// vim: set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab
