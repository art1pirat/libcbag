// Copyright (C) 2018 Andreas Romeyke (art1@andreas-romeyke.de), 2018.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include "payloadmanifest.hpp"
#include <iostream>
#include <fstream>
#include <sstream>

#include <boost/filesystem.hpp>
//#include <filesystem> // c++17

//namespace fs = std::filesystem;
namespace fs = boost::filesystem;

using namespace std;


Payloadmanifest::Payloadmanifest( const string& basedir ) : Manifest::Manifest( basedir, "manifest-") {
  
}

bool Payloadmanifest::validate() {
  bool is_valid = true;
  bool tmp = Manifest::validate();
  if (!tmp) { is_valid = false; }
  // check if at least one payload file exist
  if (this->manifest_algorithm_files.empty()) {
    this->log << "Bagit payloadmanifest count greater zero expected, but " << to_string(this->manifest_algorithm_files.size()) << " found" << endl;
    is_valid = false;
  }
  return is_valid;
}


// vim: set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab
