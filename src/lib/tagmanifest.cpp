// Copyright (C) 2018 Andreas Romeyke (art1@andreas-romeyke.de), 2018.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include "tagmanifest.hpp"
#include <iostream>
#include <fstream>
#include <sstream>

#include <boost/filesystem.hpp>
//#include <filesystem> // c++17

//namespace fs = std::filesystem;
namespace fs = boost::filesystem;

using namespace std;


Tagmanifest::Tagmanifest( const string& basedir ) : Manifest::Manifest (basedir, "tagmanifest-") {
}

multimap<checksum_string_t,filename_t> Tagmanifest::get_checksum_file_pairs(checksum_algorithms alg) {
  //log << "DEBUG: begin Tagmanifest::get_checksum_file_pairs" << endl;
  multimap<checksum_string_t,filename_t> checksum_file_pairs = Manifest::get_checksum_file_pairs(alg);
  //log << "DEBUG: called super->get_checksum_file_pairs" << endl;

  for (auto & checksum_file_pair : checksum_file_pairs) {
      // log << "DEBUG: TAGMANIFEST found 'data/' in the beginning of file '"<<it->second<<"' in tagmanifest file '"<< this->manifest_algorithm_files[ alg] << endl;
    if (checksum_file_pair.second.find( "data/" ) == 0) {
      log << "found 'data/' in the beginning of file '"<<checksum_file_pair.second<<"' in tagmanifest file '"<< this->manifest_algorithm_files[ alg] << endl;
    }
  }
  //log << "TAGMANIFEST get_checksum_file_pairs, calling debug()" << endl;
  //this->debug();
  //log << "TAGMANIFEST get_checksum_file_pairs, finished" << endl;
  return checksum_file_pairs;
}

bool Tagmanifest::has_tagmanifest() {
  return Manifest::exist_manifest_files;
}

// vim: set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab

