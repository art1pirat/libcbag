// Copyright (C) 2018 Andreas Romeyke (art1@andreas-romeyke.de), 2018.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include "bag.hpp"

using namespace std;
#include <iostream>
#include <string>

int main(int argc, const char  *argv[]) {
  if (argc != 3) {
    cout << "you need at least two arguments:" << endl;
    cout << "\t<bagit> a target dir" << endl;
    cout << "\t<data> a source dir" << endl;
    exit(-1);
  }
  const string bagit_dir = argv[1];
  const string data_dir = argv[2];
  Bag bag( bagit_dir );
  bag.reset_logstream();
  Payload payload( bagit_dir );
  payload.import_data_dir( data_dir );
  bag.add_payload( &payload );
  Bagmetadata bagmetadata( bagit_dir );
  Checksum checksum;
  oxum_t oxum = checksum.oxum_of_filelist( payload.get_all_absolute_paths() );
  bagmetadata.set_PayloadOxum( oxum );
  bagmetadata.set_SourceOrganization( "Test University" );
  bag.add_bagmetadata( &bagmetadata );
  bag.validate();
  bag.store();
  stringstream log;
  bag.get_logstream( log );
  cout << log.str() << endl;

  return 0;
}
// vim: set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab


