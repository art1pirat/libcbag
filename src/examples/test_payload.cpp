// Copyright (C) 2018 Andreas Romeyke (art1@andreas-romeyke.de), 2018.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include "payload.hpp"

using namespace std;
#include <iostream>

int main() {
  Payload p( "./CMakeFiles" );
  //list<string> files = p.get_all_absolute_paths();
  list<string> files = p.get_all_relative_paths();
  list<string>::iterator i;
  for (i=files.begin(); i!=files.end(); i++) {
    cout << "file/dir (rel):" << (*i) << endl;
    //cout << "file/dir (abs):" << (*i) << endl;
  }
  return 0;
}
// vim: set tabstop=4

