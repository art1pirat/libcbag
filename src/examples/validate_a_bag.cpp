// Copyright (C) 2018 Andreas Romeyke (art1@andreas-romeyke.de), 2018.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include "bag.hpp"

using namespace std;
#include <iostream>
#include <string>

int main(int argc, const char  *argv[]) {
  if (argc != 2) {
    cout << "you need at least one argument (a bagit dir)" << endl;
    exit(-1);
  }
  const string dir = argv[1];
  Bag bag( dir );
  bool is_valid = bag.validate();
  cout << "The bag '"<< dir <<"' is " << (is_valid ? "valid" : "invalid") << endl;
  if (!is_valid) {
      stringstream log;
      bag.get_logstream( log );
      cout << log.str() << endl;
  }
  return 0;
}
// vim: set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab

