// Copyright (C) 2018 Andreas Romeyke (art1@andreas-romeyke.de), 2018.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#ifndef LIBCBAG_PAYLOADMANIFEST
#define LIBCBAG_PAYLOADMANIFEST
#include <string>
#include <list>
#include <map>
#include "checksum.hpp"
#include "manifest.hpp"

using namespace std;

class Payloadmanifest : public Manifest {
  public:
    explicit Payloadmanifest( const string& basedir );
    bool validate();
    // bool store( string basedir, list<string> & files );
};

#endif
// vim: set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab
