// Copyright (C) 2018 Andreas Romeyke (art1@andreas-romeyke.de), 2018.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#ifndef LIBCBAG
#define LIBCBAG
#include <string>
#include <sstream>
#include "manifest.hpp"
#include "payload.hpp"
#include "payloadmanifest.hpp"
#include "tagmanifest.hpp"
#include "bagmetadata.hpp"
#include "fetchfile.hpp"
#include "othertags.hpp"

using namespace std;


class Bag {
  private:
    string base_dir; // from where the bag was loaded, optional
    stringstream log;
    int bagit_version_major;
    int bagit_version_minor;
    string tag_file_character_encoding;
    class Payload *payload_p;
    class Payloadmanifest *payloadmanifest_p;
    // next elements are optional
    class Tagmanifest *tagmanifest_p;
    class Bagmetadata *bagmetadata_p;
    class Fetchfile *fetchfile_p{};
    class Othertags *othertags_p{};
  public:
    explicit Bag(string dfname);
    list<string> get_all_bag_files();
    bool store( const string& basedir);
    bool store();
    bool validate();
    void add_payload(Payload * payload_p);
    void add_bagmetadata( Bagmetadata * bagmetadata_p);
    void add_fetchfile( Fetchfile * fetchfile_p);
    void add_othertags( Othertags * othertags_p);
    void get_logstream( stringstream & log);
    void reset_logstream();
};
#endif
// vim: set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab
