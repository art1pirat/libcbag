// Copyright (C) 2018 Andreas Romeyke (art1@andreas-romeyke.de), 2018.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#ifndef LIBCBAG_FETCHFILE
#define LIBCBAG_FETCHFILE
#include <string>
#include <list>
#include <sstream>
using namespace std;
typedef string url_t;
typedef string filename_t;
typedef struct {
	url_t url;
	size_t size;
	filename_t filename;
} fetch_t;
class Fetchfile{
  private:
    string basedir;
    list<fetch_t> entries;
    stringstream log;
    bool exist_fetchfile;
  public:
    Fetchfile( const string& tmpbasedir );
    bool has_fetchfile() const;
    list<fetch_t> get_entries();
    bool set_entries( list<fetch_t> &);
    bool fetch(const fetch_t&);
    bool fetch_all_entries();
    bool add_fetch_entry( fetch_t);
    bool remove_fetch_entry (fetch_t);
    bool validate();
    bool store( string tmpbasedir);
    void get_logstream( stringstream & log);
    void reset_logstream();
};

#endif
// vim: set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab
