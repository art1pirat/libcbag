// Copyright (C) 2018 Andreas Romeyke (art1@andreas-romeyke.de), 2018.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#ifndef LIBCBAG_PAYLOAD
#define LIBCBAG_PAYLOAD
#include <string>
#include <list>
#include <boost/filesystem.hpp>
#include <sstream>
//namespace fs = std::filesystem;
namespace fs = boost::filesystem;

using namespace std;
class Payload{
  private:
    string basedir;
    void scan_dir_recursively( const fs::path& directory, list<fs::path> &paths );
    stringstream log;
  public:
    explicit Payload( const string& tmpbasedir );
    list<string> get_all_relative_paths();
    list<string> get_all_absolute_paths();
    bool has_file( string filename );
    bool add_file( string sourcefile, string target_file );
    bool remove_file( string file );
    bool import_data_dir( const string& dirname ); /** imports a given dir as data/-dir */
    bool validate();
    bool store( const string& tmpbasedir);
    void get_logstream( stringstream & log);
    void reset_logstream();
};
#endif
// vim: set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab

