// Copyright (C) 2018 Andreas Romeyke (art1@andreas-romeyke.de), 2018.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#ifndef LIBCBAG_CHECKSUM
#define LIBCBAG_CHECKSUM
#include <sstream>
#include <list>
#include <string>
#include <openssl/md5.h>
#include <openssl/sha.h>
using namespace std;

typedef struct {
  long unsigned int octetcount;
  long unsigned int streamcount;
} oxum_t;

//enum Checksum_algorithms {md5=1, sha1, sha256};
#define SequentialEnum(Name,...) \
enum Name { __VA_ARGS__ }; \
namespace \
{ \
    const std::initializer_list<Name> Name##List { __VA_ARGS__ }; \
};

SequentialEnum(checksum_algorithms,
    md5,
    sha1,
    sha256,
    sha512,
);

string string_of_algorithm( checksum_algorithms alg);

class Checksum {
  private:
    stringstream log;
  public:
    //Checksum();
    bool is_algorithm_supported( checksum_algorithms alg );
    string checksum_of_file(const string& filepath, checksum_algorithms alg);
    oxum_t oxum_of_file(const string& filepath);
    oxum_t oxum_of_filelist( list<string> files);
    void get_logstream( stringstream & log);
    void reset_logstream();
};

#endif
// vim: set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab
