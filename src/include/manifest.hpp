// Copyright (C) 2018 Andreas Romeyke (art1@andreas-romeyke.de), 2018.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#ifndef LIBCBAG_MANIFEST
#define LIBCBAG_MANIFEST
#include <string>
#include <map>
#include <sstream>
#include "checksum.hpp"
using namespace std;
typedef string filename_t;
typedef string checksum_string_t;

class Manifest{
  protected:
    string basedir;
    stringstream log;
    string base_manifest_file_prefix;
    map<checksum_algorithms,filename_t> manifest_algorithm_files;
    bool exist_manifest_files;
  public:
    Manifest(const string& tmpbasedir, const string& file_prefix );
    virtual multimap<checksum_string_t,filename_t> get_checksum_file_pairs(checksum_algorithms alg);
    list<filename_t> get_checksummed_files();
    virtual bool validate();
    bool store(const string& tmpbasedir, list<string> & files);
    void get_logstream( stringstream & log);
    void reset_logstream();
};

#endif
// vim: set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab
