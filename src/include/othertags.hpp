// Copyright (C) 2018 Andreas Romeyke (art1@andreas-romeyke.de), 2018.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#ifndef LIBCBAG_OTHERTAGS
#define LIBCBAG_OTHERTAGS
#include <string>
#include <sstream>
#include <list>
using namespace std;
class Othertags{
  private:
    stringstream log;
  public:
    Othertags( const string& basedir);
    static bool store( const string& basedir);
    bool set_other_tag_files( list<string> &);
    list<string> get_other_tag_files();
    void get_logstream( stringstream & log);
    void reset_logstream();
};

#endif
// vim: set tabstop=4
