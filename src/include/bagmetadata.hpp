// Copyright (C) 2018 Andreas Romeyke (art1@andreas-romeyke.de), 2018.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#ifndef LIBCBAG_BAGMETADATA
#define LIBCBAG_BAGMETADATA
#include <string>
#include <map>
#include <sstream>
#include <ctime>
#include "checksum.hpp"
using namespace std;

enum {
  SourceOrganization,
  OrganizationAddress,
  ContactName,
  ContactPhone,
  ContactEmail,
  ExternalDescription,
  BaggingDate,
  ExternalIdentifier,
  BagSize,
  PayloadOxum,
  BagGroupIdentifier,
  BagCount,
  InternalSenderIdentifier,
  InternalSenderDescription,
};

class Bagmetadata{
  private:
    map<string,string> metadata;
    bool exist_bagmetadata_file;
    stringstream log;

  public:
    explicit Bagmetadata( const string& basedir );

    string get_SourceOrganization();
    string get_OrganizationAddress();
    string get_ContactName();
    string get_ContactPhone();
    string get_ContactEmail();
    string get_ExternalDescription();
    string get_BaggingDate();
    string get_ExternalIdentifier();
    string get_BagSize();
    oxum_t get_PayloadOxum();
    string get_BagGroupIdentifier();
    string get_BagCount();
    string get_InternalSenderIdentifier();
    string get_InternalSenderDescription();


    bool has_SourceOrganization();
    bool has_OrganizationAddress();
    bool has_ContactName();
    bool has_ContactPhone();
    bool has_ContactEmail();
    bool has_ExternalDescription();
    bool has_BaggingDate();
    bool has_ExternalIdentifier();
    bool has_BagSize();
    bool has_PayloadOxum();
    bool has_BagGroupIdentifier();
    bool has_BagCount();
    bool has_InternalSenderIdentifier();
    bool has_InternalSenderDescription();

    void set_SourceOrganization( const string& );
    void set_OrganizationAddress( const string& );
    void set_ContactName( const string& );
    void set_ContactPhone( const string& );
    void set_ContactEmail( const string& );
    void set_ExternalDescription( const string& );
    void set_BaggingDate( const string& );
    void set_BaggingDate( time_t );
    void set_ExternalIdentifier( const string& );
    void set_BagSize( const string& );
    void set_BagSize(long long unsigned int bytes); // count of bytes, the method calcs the human readable string
    void set_PayloadOxum( oxum_t );
    void set_BagGroupIdentifier( const string& );
    void set_BagCount( const string& );
    void set_InternalSenderIdentifier( const string& );
    void set_InternalSenderDescription( const string& );

    map<string,string> get_metadata();
    void set_metadata(map<string,string> &);
    bool has_metadata() const;

    bool validate();
    bool store( string basedir);

    void get_logstream( stringstream & log);
    void reset_logstream();
};

#endif
// vim: set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab
