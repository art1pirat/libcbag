#define BOOST_TEST_MODULE TestTagManifest
#include <boost/test/unit_test.hpp>
#include <iostream>
#include <string>
#include <utility>

#include "tagmanifest.hpp"

BOOST_AUTO_TEST_SUITE(testbag_suite);
using namespace std;



bool check_constructor(string dir) {
  try {
    Tagmanifest p( std::move(dir) );
    return true;
  } catch (exception &e) {
    return false;
  }
}

bool check_validation(string dir) {
       Tagmanifest p( std::move(dir) );
       bool res = p.validate();
       return res;
}

bool check_get_checksum_file_pairs() {
  Tagmanifest p( "../testbags/bag_minimal_ok/" );
  multimap<checksum_string_t,filename_t> results = p.get_checksum_file_pairs( md5 );
  multimap<checksum_string_t,filename_t> expected = {
    {"939f3a18385ca2975ed6a02f4a180758", "manifest-md5.txt"},
    {"eaa2c609ff6371712f623f5531945b44", "bagit.txt"},
  };
  multimap<string,string>::iterator i;
  bool is_valid = true;
  for (i=expected.begin(); i!=expected.end() ; i++) {
      cout << "Expected: (" << (i->first) << "," << (i->second) << ")" << endl;
      auto idx_first_match = results.find(i->first );
      if (idx_first_match != results.end()) { // file found
          if (idx_first_match->first != i->first) {
            cout << "Results: (" << (idx_first_match->first) << "," << (idx_first_match->second) << ")" << endl;
            is_valid=false; break;
          }
      } else {
          cout << "Results: not found" << endl;
          is_valid=false; break;
      }
  }
  return is_valid;
}

bool check_get_checksummed_files() {
  Tagmanifest p( "../testbags/bag_minimal_ok/" );
  list<string> results = p.get_checksummed_files();
  list<string> expected {
	  "../testbags/bag_minimal_ok/bag-info.txt"
  };
  results.sort();
  expected.sort();
  list<string>::iterator i;
  list<string>::iterator j;
  bool is_valid = true;
  for (i=results.begin(), j=expected.begin(); i!=results.end() && j!=expected.end(); i++, j++) {
    if ((*i) != (*j)) {
         cout << "Expected: "<< (*j) << " Result:"<< (*i) << endl;
         is_valid = false;
         break;
       }
  }
  return is_valid;
}

bool check_has_tagmanifest(string dir) {
  Tagmanifest p( std::move(dir) );
  return p.has_tagmanifest();
}


BOOST_AUTO_TEST_CASE(constructor1) {BOOST_TEST(check_constructor(""));};
BOOST_AUTO_TEST_CASE(constructor2) {BOOST_TEST(check_constructor("./"));};
BOOST_AUTO_TEST_CASE(constructor3) {BOOST_TEST(check_constructor("/tmp/"));};
BOOST_AUTO_TEST_CASE(check_validation_ok) {BOOST_TEST(check_validation("../testbags/bag_minimal_ok/"));};
BOOST_AUTO_TEST_CASE(check_checksum_file_pairs) {BOOST_TEST( check_get_checksum_file_pairs()); };
BOOST_AUTO_TEST_CASE(check_checksummed_files) { BOOST_TEST(check_get_checksummed_files()); };
BOOST_AUTO_TEST_CASE(check_tag_manifest) {BOOST_TEST(check_has_tagmanifest("../testbags/bag_minimal_ok/"));};

BOOST_AUTO_TEST_SUITE_END();

// vim: set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab
