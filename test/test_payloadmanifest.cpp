#define BOOST_TEST_MODULE TestPayloadManifest
#include <boost/test/unit_test.hpp>
#include <iostream>
#include <string>
#include <utility>

#include "payloadmanifest.hpp"
BOOST_AUTO_TEST_SUITE(testbag_suite);

using namespace std;

bool check_constructor(string dir) {
  try {
    Payloadmanifest p( std::move(dir) );
    return true;
  } catch (exception &e) {
    return false;
  }
}

bool check_validation(string dir) {
       Payloadmanifest p( std::move(dir) );
       bool res = p.validate();
       return res;
}

bool check_get_checksum_file_pairs() {
  Payloadmanifest p( "../testbags/bag_minimal_ok/" );
  multimap<checksum_string_t,filename_t> results = p.get_checksum_file_pairs( md5 );
  multimap<checksum_string_t,filename_t> expected = {
    {"e1cbb0c3879af8347246f12c559a86b5", "data/1.txt"},
    {"d41d8cd98f00b204e9800998ecf8427e", "data/3.dat"},
    {"d41d8cd98f00b204e9800998ecf8427e", "data/subdir/2.dat"},
    {"227bc609651f929e367c3b2b79e09d5b", "data/subdir/2.md5"}
  };

  multimap<string,string>::iterator i;
  multimap<string,string>::iterator j;
  bool is_valid = true;
  for (i=expected.begin(), j=results.begin(); i!=expected.end() && j!=results.end(); i++,j++) {
    if (
        ( i->first != j-> first) ||
        ( i->second != j->second)
       ) {
      cout << "Expected: (" << (i->first) << "," << (i->second) << ") Result: ("<< j->first << "," << j->second << ")" << endl;
      is_valid=false; break;
    }
  }
  return is_valid;
}

bool check_get_checksummed_files() {
  Payloadmanifest p( "../testbags/bag_minimal_ok/" );
  list<string> results = p.get_checksummed_files();
  list<string> expected {
    "../testbags/bag_minimal_ok/data/1.txt",
      "../testbags/bag_minimal_ok/data/3.dat",
      "../testbags/bag_minimal_ok/data/subdir/2.dat",
      "../testbags/bag_minimal_ok/data/subdir/2.md5",
  };
  results.sort();
  expected.sort();
  list<string>::iterator i;
  list<string>::iterator j;
  bool is_valid = true;
  for (i=results.begin(), j=expected.begin(); i!=results.end() && j!=expected.end(); i++, j++) {
    if ((*i) != (*j)) {
         cout << "Expected: "<< (*j) << " Result:"<< (*i) << endl;
         is_valid = false;
         break;
       }
  }
  return is_valid;
}


BOOST_AUTO_TEST_CASE(constructor1) {BOOST_TEST(check_constructor(""));};
BOOST_AUTO_TEST_CASE(constructor2) {BOOST_TEST(check_constructor("./"));};
BOOST_AUTO_TEST_CASE(constructor3) {BOOST_TEST(check_constructor("/tmp/"));};
BOOST_AUTO_TEST_CASE(check_validation_ok) {BOOST_TEST(check_validation("../testbags/bag_minimal_ok/"));};
BOOST_AUTO_TEST_CASE(check_validation_buggy) {BOOST_TEST(!check_validation("../testbags/1008_buggy/"));};
BOOST_AUTO_TEST_CASE(check_checksum_file_pairs) {BOOST_TEST( check_get_checksum_file_pairs()); };
BOOST_AUTO_TEST_CASE(check_checksummed_files) { BOOST_TEST(check_get_checksummed_files()); };

BOOST_AUTO_TEST_SUITE_END();
// vim: set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab
