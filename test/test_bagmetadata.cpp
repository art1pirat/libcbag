#define BOOST_TEST_MODULE TestBagMetadata
#include <boost/test/unit_test.hpp>
#include <iostream>
#include <string>
#include <utility>
#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;
#include "bagmetadata.hpp"
#include "checksum.hpp"
BOOST_AUTO_TEST_SUITE(testbag_suite);
using namespace std;

void debug( Bagmetadata * p) {
    stringstream log;
    p->get_logstream( log );
    cout << "##############################################" << endl;
    cout << "DEBUG Bagmetadata" << endl;
    cout << log.str() << endl;
    cout << "##############################################" << endl;
}

bool check_constructor(string dir) {
	try {
		Bagmetadata p( std::move(dir) );
		return true;
	} catch (exception &e) {
		return false;
	}
}

bool check_validation(string dir) {
	Bagmetadata p( std::move(dir) );
	bool res = p.validate();
	return res;
}

bool check_get_metadata(string dir) {
	Bagmetadata p( std::move(dir) );
	map<string,string> results = p.get_metadata();
	map<string,string> expected = {
		{"External-Identifier", "testbag-01"},
		{"Payload-Oxum", "38.4"},
		{"Bagging-Date", "2015-12-28"},
		{"Bag-Size", "16 B"},
		{"External-Description", "3.dat"},
		{"Foo", "bar baz\n     blurp"},
	};
	map<string, string>::iterator i;
	map<string, string>::iterator j;
	bool is_valid = true;
	for (i=expected.begin(), j=results.begin(); i!=expected.end() && j!=results.end(); i++,j++) {
		if (
				( i->first != j-> first) ||
				( i->second != j->second)
		   ) {
			cout << "Expected: (" << (i->first) << ";" << (i->second) << ")" << endl;
			cout << "Result:   ("<< j->first << ";" << j->second << ")" << endl;
			is_valid=false; break;
		}
	}
	return is_valid;
}

bool check_has_metadata(string dir) {
	Bagmetadata p( std::move(dir) );
	return p.has_metadata();
}

/*
bool check_has_PayloadOxum(string dir) {
	Bagmetadata p( std::move(dir) );
	return p.has_PayloadOxum();
}

oxum_t check_get_PayloadOxum(string dir) {
	Bagmetadata p( std::move(dir) );
	return p.get_PayloadOxum();
}
*/

bool check_store( string dir_load_from, string dir_store_to ) {
	Bagmetadata p( std::move(dir_load_from) );
    fs::path f{ dir_store_to };
    fs::create_directory(f);
	bool result = p.store( std::move(dir_store_to) );
    fs::remove_all( f );
    debug( &p );
	return result;
}

// 


bool check_set_has_get_SourceOrganization(string dir, string expected) {
	Bagmetadata p( std::move(dir) );
	p.set_SourceOrganization( expected );
	if (!p.has_SourceOrganization() ) {
		return false;
	}

	string result = p.get_SourceOrganization();
	return (0 == expected.compare( result ) );
}
bool check_set_has_get_OrganizationAddress(string dir, string expected) {
	Bagmetadata p( std::move(dir) );
	p.set_OrganizationAddress( expected );
	if (!p.has_OrganizationAddress() ) {
		return false;
	}

	string result = p.get_OrganizationAddress();
	return (0 == expected.compare( result ) );
}
bool check_set_has_get_ContactName(string dir, string expected) {
	Bagmetadata p( std::move(dir) );
	p.set_ContactName( expected );
	if (!p.has_ContactName() ) {
		return false;
	}

	string result = p.get_ContactName();
	return (0 == expected.compare( result ) );
}
bool check_set_has_get_ContactPhone(string dir, string expected) {
	Bagmetadata p( std::move(dir) );
	p.set_ContactPhone( expected );
	if (!p.has_ContactPhone() ) {
		return false;
	}

	string result = p.get_ContactPhone();
	return (0 == expected.compare( result ) );
}
bool check_set_has_get_ContactEmail(string dir, string expected) {
	Bagmetadata p( std::move(dir) );
	p.set_ContactEmail( expected );
	if (!p.has_ContactEmail() ) {
		return false;
	}

	string result = p.get_ContactEmail();
	return (0 == expected.compare( result ) );
}
bool check_set_has_get_ExternalDescription(string dir, string expected) {
	Bagmetadata p( std::move(dir) );
	p.set_ExternalDescription( expected );
	if (!p.has_ExternalDescription() ) {
		return false;
	}

	string result = p.get_ExternalDescription();
	return (0 == expected.compare( result ) );
}
bool check_set_has_get_BaggingDate(string dir, string expected) {
	Bagmetadata p( std::move(dir) );
	p.set_BaggingDate( expected );
	if (!p.has_BaggingDate() ) {
		return false;
	}

	string result = p.get_BaggingDate();
	return (0 == expected.compare( result ) );
}
bool check_set_has_get_ExternalIdentifier(string dir, string expected) {
	Bagmetadata p( std::move(dir) );
	p.set_ExternalIdentifier( expected );
	if (!p.has_ExternalIdentifier() ) {
		return false;
	}

	string result = p.get_ExternalIdentifier();
	return (0 == expected.compare( result ) );
}
bool check_set_has_get_BagSize(string dir, string expected) {
	Bagmetadata p( std::move(dir) );
	p.set_BagSize( expected );
	if (!p.has_BagSize() ) {
		return false;
	}

	string result = p.get_BagSize();
	return (0 == expected.compare( result ) );
}
bool check_set_has_get_PayloadOxum(string dir, oxum_t expected) {
	Bagmetadata p( std::move(dir) );
	p.set_PayloadOxum( expected );
	if (!p.has_PayloadOxum() ) {
		return false;
	}

	oxum_t result = p.get_PayloadOxum();

	return (expected.octetcount == result.octetcount 
			&& expected.streamcount == result.streamcount
	       );
}
bool check_set_has_get_BagGroupIdentifier(string dir, string expected) {
	Bagmetadata p( std::move(dir) );
	p.set_BagGroupIdentifier( expected );
	if (!p.has_BagGroupIdentifier() ) {
		return false;
	}

	string result = p.get_BagGroupIdentifier();
	return (0 == expected.compare( result ) );
}
bool check_set_has_get_BagCount(string dir, string expected) {
	Bagmetadata p( std::move(dir) );
	p.set_BagCount( expected );
	if (!p.has_BagCount() ) {
		return false;
	}

	string result = p.get_BagCount();
	return (0 == expected.compare( result ) );
}
bool check_set_has_get_InternalSenderIdentifier(string dir, string expected) {
	Bagmetadata p( std::move(dir) );
	p.set_InternalSenderIdentifier( expected );
	if (!p.has_InternalSenderIdentifier() ) {
		return false;
	}

	string result = p.get_InternalSenderIdentifier();
	return (0 == expected.compare( result ) );
}
bool check_set_has_get_InternalSenderDescription(string dir, string expected) {
	Bagmetadata p( std::move(dir) );
	p.set_InternalSenderDescription( expected );
	if (!p.has_InternalSenderDescription() ) {
		return false;
	}

	string result = p.get_InternalSenderDescription();
	return (0 == expected.compare( result ) );
}


//
//
//
//
BOOST_AUTO_TEST_CASE(constructor1) {BOOST_TEST(check_constructor(""));};
BOOST_AUTO_TEST_CASE(constructor2) {BOOST_TEST(check_constructor("./"));};
BOOST_AUTO_TEST_CASE(constructor3) {BOOST_TEST(check_constructor("/tmp/"));};
BOOST_AUTO_TEST_CASE(check_metadata_exists) {BOOST_TEST( check_has_metadata("../testbags/bag_minimal_ok/")); };

BOOST_AUTO_TEST_CASE(check_metadata) {BOOST_TEST( check_get_metadata("../testbags/bag_minimal_ok/")); };
BOOST_AUTO_TEST_CASE(check_oxum) {
	oxum_t expected{};
	expected.octetcount=38;
	expected.streamcount=4;
	BOOST_TEST( check_set_has_get_PayloadOxum("../testbags/bag_minimal_ok/", expected));
};




BOOST_AUTO_TEST_CASE(check_validation_ok) {BOOST_TEST(check_validation("../testbags/bag_minimal_ok/"));};
BOOST_AUTO_TEST_CASE(check_store_ok) {BOOST_TEST(check_store("../testbags/bag_minimal_ok/", "/tmp/testbag/"));};



//BOOST_AUTO_TEST_CASE(check_has_PayloadOxum_ok)               {BOOST_TEST(check_has_PayloadOxum("../testbags_bag_minimal_ok/"));};
//BOOST_AUTO_TEST_CASE(check_get_PayloadOxum_ok)               {BOOST_TEST(check_get_PayloadOxum("../testbags_bag_minimal_ok/"));};
//BOOST_AUTO_TEST_CASE(check_store_ok)                         {BOOST_TEST(check_store("../testbags_bag_minimal_ok/"));};
BOOST_AUTO_TEST_CASE(check_set_has_get_SourceOrganization_ok)        {BOOST_TEST(check_set_has_get_SourceOrganization("../testbags_bag_minimal_ok/", "test"));};
BOOST_AUTO_TEST_CASE(check_set_has_get_OrganizationAddress_ok)       {BOOST_TEST(check_set_has_get_OrganizationAddress("../testbags_bag_minimal_ok/", "test"));};
BOOST_AUTO_TEST_CASE(check_set_has_get_ContactName_ok)               {BOOST_TEST(check_set_has_get_ContactName("../testbags_bag_minimal_ok/", "test"));};
BOOST_AUTO_TEST_CASE(check_set_has_get_ContactPhone_ok)              {BOOST_TEST(check_set_has_get_ContactPhone("../testbags_bag_minimal_ok/", "test"));};
BOOST_AUTO_TEST_CASE(check_set_has_get_ContactEmail_ok)              {BOOST_TEST(check_set_has_get_ContactEmail("../testbags_bag_minimal_ok/", "test"));};
BOOST_AUTO_TEST_CASE(check_set_has_get_ExternalDescription_ok)       {BOOST_TEST(check_set_has_get_ExternalDescription("../testbags_bag_minimal_ok/", "test"));};
BOOST_AUTO_TEST_CASE(check_set_has_get_BaggingDate_ok)               {BOOST_TEST(check_set_has_get_BaggingDate("../testbags_bag_minimal_ok/", "test"));};
BOOST_AUTO_TEST_CASE(check_set_has_get_ExternalIdentifier_ok)        {BOOST_TEST(check_set_has_get_ExternalIdentifier("../testbags_bag_minimal_ok/", "test"));};
BOOST_AUTO_TEST_CASE(check_set_has_get_BagSize_ok)                   {BOOST_TEST(check_set_has_get_BagSize("../testbags_bag_minimal_ok/", "test"));};
BOOST_AUTO_TEST_CASE(check_set_has_get_PayloadOxum_ok)               {BOOST_TEST(check_set_has_get_PayloadOxum("../testbags_bag_minimal_ok/", {2,1}));};
BOOST_AUTO_TEST_CASE(check_set_has_get_BagGroupIdentifier_ok)        {BOOST_TEST(check_set_has_get_BagGroupIdentifier("../testbags_bag_minimal_ok/", "test"));};
BOOST_AUTO_TEST_CASE(check_set_has_get_BagCount_ok)                  {BOOST_TEST(check_set_has_get_BagCount("../testbags_bag_minimal_ok/", "test"));};
BOOST_AUTO_TEST_CASE(check_set_has_get_InternalSenderIdentifier_ok)  {BOOST_TEST(check_set_has_get_InternalSenderIdentifier("../testbags_bag_minimal_ok/", "test"));};
BOOST_AUTO_TEST_CASE(check_set_has_get_InternalSenderDescription_ok) {BOOST_TEST(check_set_has_get_InternalSenderDescription("../testbags_bag_minimal_ok/", "test"));};
BOOST_AUTO_TEST_SUITE_END();

// vim: set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab
