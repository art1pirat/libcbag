#define BOOST_TEST_MODULE Mytest
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>
#include <utility>
#include <iostream>

#include "bag.hpp"
BOOST_AUTO_TEST_SUITE(testbag_suite);
using namespace std;
#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

struct TARGETFIXTURE {
    TARGETFIXTURE() : targetdir("/tmp/testdir") { BOOST_TEST_MESSAGE("setup targetdir"); }
    ~TARGETFIXTURE() {
        fs::path f { targetdir };
        fs::remove_all( f );
        BOOST_TEST_MESSAGE("teardown targetdir");
    }
    string targetdir;
};


bool check_constructor(string dir) {
    try {
        Bag p( std::move(dir) );
        return true;
    } catch (exception &e) {
        return false;
    }
}

bool check_validation(string dir) {
    Bag p( std::move(dir) );
    return p.validate();
}

bool check_store( string dir_load_from, string dir_store_to ) {
    Bag p( std::move(dir_load_from) );
    bool result = p.store ( dir_store_to );
    // clean dir_store_to
    fs::path f { dir_store_to };
    fs::remove_all( f );
    return result;
}

bool check_get_all_bag_files(string dir) {
    Bag p( std::move(dir) );
    list<string> results = p.get_all_bag_files();
    list<string> expected {
        "bagit.txt",
        "bag-info.txt"
    };
    bool is_valid = true;
    list<string>::iterator i;
    list<string>::iterator j;
    for (i=results.begin(), j=expected.begin(); i!=results.end() && j!=expected.end(); i++, j++) {
        if ((*i) != (*j)) {
            cout << "Expected: "<< (*j) << " Result:"<< (*i) << endl;
            is_valid = false;
            break;
        }
    }
    return is_valid;
}



BOOST_AUTO_TEST_CASE(constructor1) {
    BOOST_TEST(check_constructor(""));
};
BOOST_AUTO_TEST_CASE(constructor2) {
    BOOST_TEST(check_constructor("./"));
};
BOOST_AUTO_TEST_CASE(constructor3) {
    BOOST_TEST(check_constructor("../testbags/bag_minimal_ok/"));
};
BOOST_AUTO_TEST_CASE(constructor3_old) {
    BOOST_TEST(check_constructor("../testbags/bag_minimal_v0.97/"));
};
BOOST_AUTO_TEST_CASE(check_validation_ok) {
    BOOST_TEST(check_validation("../testbags/bag_minimal_ok/"));
};
BOOST_AUTO_TEST_CASE(check_validation_ok_old) {
    BOOST_TEST(check_validation("../testbags/bag_minimal_v0.97/"));
};
BOOST_AUTO_TEST_CASE(check_validation_buggy) {
    BOOST_TEST(!check_validation("../testbags/1008_buggy/"));
};
BOOST_AUTO_TEST_CASE(check_get_all_bag_files_ok) {
    BOOST_TEST(check_get_all_bag_files("../testbags/bag_minimal_ok"));
};
BOOST_AUTO_TEST_CASE(check_get_all_bag_files_ok_old) {
    BOOST_TEST(check_get_all_bag_files("../testbags/bag_minimal_v0.97"));
};

void test_valid_bagit_dirs () {
  list<string> valid_dirs {
    "valid_md5",
      "valid_md5_invalid_sha1_invalid_sha256_invalid_sha512",
      "valid_md5_invalid_sha1_invalid_sha256_valid_sha512",
      "valid_md5_invalid_sha1_valid_sha256_invalid_sha512",
      "valid_md5_invalid_sha1_valid_sha256_valid_sha512",
      "valid_md5_valid_sha1_invalid_sha256_invalid_sha512",
      "valid_md5_valid_sha1_invalid_sha256_valid_sha512",
      "valid_md5_valid_sha1_valid_sha256_invalid_sha512",
      "valid_md5_valid_sha1_valid_sha256_valid_sha512",
      "valid_multiple_valid_checksums",
      "valid_sha1",
      "valid_sha256",
      "valid_sha512",
      "valid_with_additional_meta_dir"
  };
  for (auto i=valid_dirs.begin(); i!=valid_dirs.end(); i++) {
      string bagdir = "../testbags/" + *i;
      BOOST_TEST(check_validation(bagdir), bagdir);
  }
}

void test_invalid_bagit_dirs () {
  list<string> invalid_dirs {
    "invalid_data_file_in_tagmanifest",
      "invalid_duplicate_bagcount",
      "invalid_duplicate_bagging_date",
      "invalid_duplicate_baggroup",
      "invalid_duplicate_bagsize",
      "invalid_duplicate_entries_in_manifest",
      "invalid_duplicate_entries_in_tagmanifest",
      "invalid_duplicate_payload_oxum",
      "invalid_empty_manifest_but_entries_in_data",
      "invalid_empty_tagmanifest",
      "invalid_md5",
      "invalid_md5_invalid_sha1_invalid_sha256_invalid_sha512",
      "invalid_md5_invalid_sha1_invalid_sha256_valid_sha512",
      "invalid_md5_invalid_sha1_valid_sha256_invalid_sha512",
      "invalid_md5_invalid_sha1_valid_sha256_valid_sha512",
      "invalid_md5_valid_sha1_invalid_sha256_invalid_sha512",
      "invalid_md5_valid_sha1_invalid_sha256_valid_sha512",
      "invalid_md5_valid_sha1_valid_sha256_invalid_sha512",
      "invalid_md5_valid_sha1_valid_sha256_valid_sha512",
      "invalid_missed_baginfo",
      "invalid_missed_baginfo_in_tagmanifest",
      "invalid_missed_bagit",
      "invalid_missed_bagit_in_tagmanifest",
      "invalid_missed_bagit_version",
      "invalid_missed_data_dir",
      "invalid_missed_data_subdir",
      "invalid_missed_encoding",
      "invalid_missed_manifest",
      "invalid_missed_manifest_in_tagmanifest",
      "invalid_missed_referenced_data_file",
      "invalid_missed_referenced_file_in_subdir",
      "invalid_missed_tagmanifest",
      "invalid_payload_oxum",
      "invalid_sha1",
      "invalid_sha256",
      "invalid_sha512",
      "invalid_tagmanifest_files_with_different_entries",
      "outdated_bagit_version",
      "unreferenced_files_in_bag"
  };

  for (auto i=invalid_dirs.begin(); i!=invalid_dirs.end(); i++) {
      string bagdir = "../testbags/" + *i;
      BOOST_TEST(!check_validation(bagdir), bagdir);
  }
}
BOOST_AUTO_TEST_CASE(check_valid_dirs) {
  test_valid_bagit_dirs();
}
BOOST_AUTO_TEST_CASE(check_invalid_dirs) {
  test_invalid_bagit_dirs();
}

BOOST_FIXTURE_TEST_CASE(check_store_ok, TARGETFIXTURE) {
  BOOST_TEST(check_store("../testbags/bag_minimal_ok/", targetdir));
};


BOOST_AUTO_TEST_SUITE_END();

// vim: set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab
