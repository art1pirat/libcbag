#define BOOST_TEST_MODULE TestFetchFile
#include <boost/test/unit_test.hpp>
#include <iostream>
#include <string>
#include <list>
#include <utility>
#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include "fetchfile.hpp"
BOOST_AUTO_TEST_SUITE(testbag_suite);
using namespace std;


bool check_constructor(string dir) {
  try {
    Fetchfile p( std::move(dir) );
    return true;
  } catch (exception &e) {
    return false;
  }
}

bool check_validation(string dir) {
       Fetchfile p( std::move(dir) );
       return p.validate();
}

bool check_store( string dir_load_from, string dir_store_to ) {
	Fetchfile p( std::move(dir_load_from) );
    fs::path f{ dir_store_to };
    fs::create_directory(f);
	bool result = p.store( std::move(dir_store_to) );
    fs::remove_all( f );
	return result;
}

bool check_get_fetchfile(string dir) {
	Fetchfile p( std::move(dir) );
	list<fetch_t> results = p.get_entries();
	list<fetch_t> expected;
	list<fetch_t>::iterator i;
	list<fetch_t>::iterator j;
	bool is_valid = true;
	for (i=expected.begin(), j=results.begin(); i!=expected.end() && j!=results.end(); i++,j++) {
		if (
				( i->url != j-> url) ||
				( i->size != j->size) ||
				( i->filename != j->filename)
		   ) {
			cout << "Expected: (" << (i->url) << ";" << (i->size) << ";"<< (i->filename)<< ")" << endl;
			cout << "Result:   ("<< (j->url) << ";" << (j->size) << ";" << (j->filename)<< ")" << endl;
			is_valid=false; break;
		}
	}
	return is_valid;
}


bool check_has_fetchfile(string dir) {
  Fetchfile p( std::move(dir) );
  return p.has_fetchfile();
}


BOOST_AUTO_TEST_CASE(constructor1) {BOOST_TEST(check_constructor(""));};
BOOST_AUTO_TEST_CASE(constructor2) {BOOST_TEST(check_constructor("./"));};
BOOST_AUTO_TEST_CASE(constructor3) {BOOST_TEST(check_constructor("/tmp/"));};
BOOST_AUTO_TEST_CASE(check_validation_ok) {BOOST_TEST(check_validation("../testbags/bag_minimal_ok/"));};
BOOST_AUTO_TEST_CASE(check_validation_buggy) {BOOST_TEST(check_validation("../testbags/1008_buggy/"));};
BOOST_AUTO_TEST_CASE(check_fetchfile_exists) {BOOST_TEST(! check_has_fetchfile("../testbags/bag_minimal_ok/")); };
BOOST_AUTO_TEST_CASE(check_fetchfile) {BOOST_TEST( check_get_fetchfile("../testbags/bag_minimal_ok/")); };

BOOST_AUTO_TEST_CASE(check_store_ok) {BOOST_TEST(check_store("../testbags/bag_minimal_ok/", "/tmp/testbag/"));};

BOOST_AUTO_TEST_SUITE_END();
// vim: set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab
