#define BOOST_TEST_MODULE TestPayload
#include <boost/test/unit_test.hpp>
#include <iostream>
#include <string>
#include <utility>

#include "payload.hpp"
BOOST_AUTO_TEST_SUITE(testbag_suite);
using namespace std;

bool check_constructor(string dir) {
  try {
    Payload p( std::move(dir) );
    return true;
  } catch (exception &e) {
    return false;
  }
}

bool check_validation(string dir) {
       Payload p( std::move(dir) );
       return p.validate();
}

bool check_relative_paths() {
  Payload p( "../testbags/bag_minimal_ok/" );
  list<string> results = p.get_all_relative_paths();
  list<string> expected {
    "data/1.txt",
    "data/3.dat",
    "data/subdir/2.dat",
    "data/subdir/2.md5",
  };
  results.sort();
  expected.sort();
  list<string>::iterator i;
  list<string>::iterator j;
  bool is_valid = true;
  for (i=results.begin(), j=expected.begin(); i!=results.end() && j!=expected.end(); i++, j++) {
    if ((*i) != (*j)) {
         cout << "Expected: "<< (*j) << " Result:"<< (*i) << endl;
         is_valid = false;
         break;
       }
  }
  return is_valid;
}

BOOST_AUTO_TEST_CASE(constructor1) {BOOST_TEST(check_constructor(""));};
BOOST_AUTO_TEST_CASE(constructor2) {BOOST_TEST(check_constructor("./"));};
BOOST_AUTO_TEST_CASE(constructor3) {BOOST_TEST(check_constructor("/tmp/"));};
BOOST_AUTO_TEST_CASE(check_validation_ok) {BOOST_TEST(check_validation("../testbags/bag_minimal_ok/"));};
BOOST_AUTO_TEST_CASE(check_validation_buggy) {BOOST_TEST(!check_validation("../testbags/1008_buggy/"));};
BOOST_AUTO_TEST_CASE(check_rel_paths) { BOOST_TEST(check_relative_paths()); };

BOOST_AUTO_TEST_SUITE_END();
// vim: set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab
